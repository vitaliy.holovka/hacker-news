import React from "react";
import Stories from "../../components/stories/stories";

const Main = () => (
  <div className="wrapper">
    <div className="story-table story-table--collapse">
      <Stories/>
    </div>
  </div>
);

export default Main;
