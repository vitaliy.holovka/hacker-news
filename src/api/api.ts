import axios from "axios";
import { TOP_STORIES, URL_API } from "../constants/api";
import { UserProps } from "../types/story";

export const getStory = (id: number) => {
  const stories = `/v0/item/${id}.json?print=pretty`;
  return stories;
};

export const getUser = (user: UserProps) => {
  const getUsr = `/v0/user/${user.by}.json?print=pretty`;
  return getUsr;
};

export const getUsers = async () => {
  const url = URL_API + TOP_STORIES;
 
  return await axios.get(url);
};

export const getStoryAsync = async (id: number) => {
  const url = URL_API + `/v0/item/${id}.json?print=pretty`;
 
  return await axios.get(url);
};

export const getUserAsync = async (user: string) => {
  const url = URL_API + `/v0/user/${user}.json?print=pretty`;
 
  return await axios.get(url);
};
