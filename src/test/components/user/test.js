import axios from 'axios';
import { getUserAsync } from '../../../api/api';

jest.mock('axios');


describe('Fetch user by id', () => {
  it('Fetches user from an Hacker News API', async () => {
    const user = {
      "about": "srikavineehari at gmail",
      "created": 1471874751,
      "id": "happy-go-lucky",
      "karma": 21928,
      "submitted": [
          28295205,
          28262044
      ]
  }

    axios.get.mockImplementationOnce(() => Promise.resolve(user));
    await expect(getUserAsync('happy-go-lucky')).resolves.toEqual(user);

    expect(axios.get).toHaveBeenCalledWith(
     "https://hacker-news.firebaseio.com/v0/user/happy-go-lucky.json?print=pretty"
    );
  });

  it('Fetches erroneously user from an API', async () => {
    const errorMessage = 'Network Error';

    axios.get.mockImplementationOnce(() =>
      Promise.reject(new Error(errorMessage)),
    );

    await expect(getUserAsync('happy-go-lucky')).rejects.toThrow(errorMessage);
  });
});