import axios from 'axios';
import { getStoryAsync } from '../../../api/api';

jest.mock('axios');


describe('Fetch story by id', () => {
  it('Fetches story from an Hacker News API', async () => {
    const story = {
      "by": "happy-go-lucky",
      "descendants": 0,
      "id": 28295205,
      "score": 4,
      "time": 1629840445,
      "title": "Math opens up a variety of rewarding careers intellectually and financially",
      "type": "story",
      "url": "https://www.sciencefocus.com/science/want-to-make-money-become-a-mathematician-seriously/"
  }

    axios.get.mockImplementationOnce(() => Promise.resolve(story));
    await expect(getStoryAsync(28295205)).resolves.toEqual(story);

    expect(axios.get).toHaveBeenCalledWith(
      "https://hacker-news.firebaseio.com/v0/item/28295205.json?print=pretty",
    );
  });

  it('Fetches erroneously story from an API', async () => {
    const errorMessage = 'Network Error';

    axios.get.mockImplementationOnce(() =>
      Promise.reject(new Error(errorMessage)),
    );

    await expect(getStoryAsync(28295205)).rejects.toThrow(errorMessage);
  });
});