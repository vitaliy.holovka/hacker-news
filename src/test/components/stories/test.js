import axios from 'axios';
import { getUsers } from '../../../api/api';
import { TOP_STORIES, URL_API } from "../../../constants/api";

jest.mock('axios');


describe('fetchData', () => {
  it('Fetches top stories from an Hacker News API', async () => {
    const data = {
      data: {
        hits: [
          {
            objectID: '1',
            title: 'a',
          },
          {
            objectID: '2',
            title: 'b',
          },
        ],
      },
    };

    axios.get.mockImplementationOnce(() => Promise.resolve(data));
    await expect(getUsers()).resolves.toEqual(data);

    expect(axios.get).toHaveBeenCalledWith(
      URL_API + TOP_STORIES,
    );
  });

  it('Fetches erroneously top stories from an API', async () => {
    const errorMessage = 'Network Error';

    axios.get.mockImplementationOnce(() =>
      Promise.reject(new Error(errorMessage)),
    );

    await expect(getUsers).rejects.toThrow(errorMessage);
  });
});