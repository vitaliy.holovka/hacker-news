import React, { useEffect, useState } from "react";
import axios from "axios";

import { getStory } from "../../api/api";
import { TOP_STORIES, URL_API } from "../../constants/api";
import Header from "../header/header";
import fields from "../../data/fields";
import SortStories from "./sortStories";
import { ISortStory } from "../../types/story";

const Stories = () => {
  const [storiesState, setStoriesState] = useState<Array<ISortStory>>();
  const [stories, setStories] = useState<Array<number>>();
  const sortStories: Array<ISortStory> = [];

  const getStories = async (): Promise<boolean> => {
    axios.get(URL_API + TOP_STORIES).then((response) => {
      if (!stories) {
        setStories(response.data?.sort(() => 0.5 - Math.random()).slice(0, 10));
      }
    });

    return true;
  };

  const getStoryById = async (id: number): Promise<boolean> => {
    axios.get(URL_API + getStory(id)).then((response) => {
      sortStories.push({ index: response.data.id, score: response.data.score });
      if (sortStories.length === 10) {
        setStoriesState(
          sortStories.sort(function (a: any, b: any) {
            return a.score - b.score;
          })
        );
      }
    });

    return true;
  };

  useEffect(() => {
    if (!storiesState) {
      getStories();
      stories?.map((index: number) => {
        getStoryById(index);
      });
    }
  }, [storiesState,stories]);

  return (
    <>
      <Header fields={fields} />
      <SortStories stories={storiesState} />
    </>
  );
};

export default Stories;
