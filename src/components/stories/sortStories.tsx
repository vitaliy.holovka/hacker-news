import React from "react";
import { StortStoriesProps } from "../../types/story";
import Story from "../story/story";

const SortStories = (data: StortStoriesProps) => {

  return (
    <>
      {data.stories?.map((story:any,index:any) => {
        return <Story key={index} id={story["index"]} />
      })}
    </>
  );
};

export default SortStories;
