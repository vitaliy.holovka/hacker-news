import React, { useEffect, useState } from "react";
import { IStory, StoryProps } from "../../types/story";
import { URL_API } from "../../constants/api";
import User from "../user/user";
import { ReactComponent as Icon } from "../../assets/logo.svg";
import { getStory } from "../../api/api";
import axios from "axios";

const Story = ({ id }: StoryProps) => {


  const [story, setStory] = useState<IStory>();
  const time = Number(story?.time);
  const date = new Date(time * 1000).toLocaleDateString("en-US");

  const getStoryById = async (id: number): Promise<boolean> => {
    axios.get(URL_API + getStory(id)).then((response) => {
      setStory(response.data);
    });

    return true;
  };

  useEffect(() => {
    if(!story){
      getStoryById(id);
    }
  },[story])
  

  return (
    <>
      <div className="story-row">
        <div className="story-cell story-cell">
          <div className="story-cell--heading">Story title</div>
          <div className="story-cell--content">{story?.title}</div>
        </div>
        <div className="story-cell url-cell">
          <div className="story-cell--heading">Story URL</div>
          <div className="story-cell--content">
            <a href={story?.url} rel="noreferrer" target="_blank">
              {story?.url}
            </a>
          </div>
        </div>
        <div className="story-cell cell">
          <div className="story-cell--heading">Story timestamp</div>
          <div className="story-cell--content">{date}</div>
        </div>
        <div className="story-cell cell">
          <div className="story-cell--heading">Story score</div>
          <div className="story-cell--content">{story?.score}</div>
        </div>

        {story?.by && <User by={story?.by} />}

        <div className="story-cell cell">
          <div className="story-cell--heading">Dummy Image</div>
          <div className="story-cell--content">
            <Icon />
          </div>
        </div>
      </div>
    </>
  );
};

export default Story;
