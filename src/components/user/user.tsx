import React, { useEffect, useState } from "react";
import { IUser, UserProps } from "../../types/story";
import { URL_API } from "../../constants/api";
import { getUser } from "../../api/api";
import axios from "axios";

const User = (by: UserProps) => {
  const [user, setUser] = useState<IUser>();

  const getUserByName = async (by: UserProps): Promise<boolean> => {
    axios.get(URL_API + getUser(by)).then((response) => {
      setUser(response.data);
    });

    return true;
  };

  useEffect(() => {
    if (!user) {
      getUserByName(by);
    }
  }, [user]);

  return (
    <>
      <div className="story-cell cell">
        <div className="story-cell--heading">Author id</div>
        <div className="story-cell--content">{user?.id}</div>
      </div>
      <div className="story-cell cell">
        <div className="story-cell--heading">Author karma score</div>
        <div className="story-cell--content">{user?.karma}</div>
      </div>
    </>
  );
};

export default User;
