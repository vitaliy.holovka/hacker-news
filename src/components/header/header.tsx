import React from "react";
import { IHeaderField, IHeaderFields } from "../../types/story";

interface IHeader {
  fields: IHeaderFields;
}
const Header = ({ fields }: IHeader) => {
  return (
    <>
      <div className="story-row story-row--head">
        {fields.map((item: IHeaderField) => (
          <div
            key={item.id}
            className={`story-cell ${
              item.id == 2 ? "url-cell" : item.id == 1 ? "story-cell" : "cell"
            } column-heading`}
          >
            {item.description}
          </div>
        ))}
      </div>
    </>
  );
};

export default Header;
