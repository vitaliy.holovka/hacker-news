export type IHeaderField = {
  id: number;
  description: string;
};

export type IHeaderFields = Array<IHeaderField>;

export type IStory = {
  id: number;
  title: string;
  url: string;
  time: string;
  score: number;
  by: string;
};

export type IUser = {
  id: string;
  karma: number;
};

export interface ISortStory {
  index: number;
  score: number;
}

export interface StoryProps {
  id: number;
}

export interface StortStoriesProps {
  stories: Array<ISortStory> | undefined;
}
export interface UserProps {
  by: string;
}
