const fields = [
  {
    id: 1,
    description: "Story Title",
  },
  {
    id: 2,
    description: "Story URL",
  },
  {
    id: 3,
    description: "Story Timestamp",
  },
  {
    id: 4,
    description: "Story Score",
  },
  {
    id: 7,
    description: "Author ID",
  },
  {
    id: 8,
    description: "Author Karma",
  },
  {
    id: 9,
    description: "Dummy Image",
  },
];

export default fields;
