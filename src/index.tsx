import React from 'react';
import ReactDOM from 'react-dom';
import Main from './pages/main/main';

import './styles/index.scss';


ReactDOM.render(<Main />, document.getElementById('root'));
