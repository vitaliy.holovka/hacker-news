Hacker News is a webpage that displays 10 random stories using the Hacker News API. The stories listed in ascending order based on the stories score.

The UI must include:
- Story title
- Story URL
- Story timestamp
- Story score
- Author id
- Author karma score
- A dummy image (not from API –just a static asset)

## The order of starting the application

Please open a terminal and write the following commands in the root folder of the project 

### 1. `npm install`

Install the dependencies in the local node_modules folder. 

### 2. `npm run start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.


### 3. `npm run test` (optional)

Launches the test runner Jest in the interactive watch mode.<br>
Jest is a JavaScript test runner, that is, a JavaScript library for creating, running, and structuring tests. 


